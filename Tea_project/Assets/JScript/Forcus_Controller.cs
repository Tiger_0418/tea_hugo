﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Forcus_Controller : MonoBehaviour {
    public Animator animator;
    public AnimationClip zoom_in;
    public AnimationClip zoom_out;
	// Use this for initialization
	void Start () {
	}
    public void ShowImage()
    {
        animator.Play(zoom_in.name);
    }
    public void unShowImage()
    {
        animator.Play(zoom_out.name);
    }
	// Update is called once per frame
	void Update () {
		
	}
}
