﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Video;

public class Screenshot : MonoBehaviour 
{
	public int superSize = 3;

	public GameObject shot;
	public GameObject replay;
	public GameObject replay_1;
	public GameObject replay_2;
	public GameObject replay_3;
	public GameObject back;
	public GameObject hint;
	public GameObject quit;
	public GameObject Timeout;

	public GameObject Scanlogo;
	public GameObject ScanHugochan;
	public GameObject ScanMadeTea;
	public GameObject ScanVideo;
	public MediaPlayerCtrl scrMedia;
	public Animator tea_2_ani;
	public GameObject logo;
	public GameObject Hugo;
	public GameObject MadeTea;
	public GameObject TeaVideo;

	public float time = 0;
	public float time_1 = 0;
	public float time_2 = 0;
	public float time_3 = 0;
	public bool timestart = false;
	public bool time_1_start = false;
	public bool time_2_start = false;
	public bool time_3_start = false;

	public int play_once = 0;

	public Forcus_Controller Forcus;

	public AudioClip gas_switch;
	public AudioClip kettle_boiling;
	public AudioClip hotwater;
	public AudioClip hot_tea;

	public AudioSource music_1;
	public AudioSource music_2;
	public AudioSource music_3;

	void Update ()
	{
		if (logo.activeSelf && timestart == false && play_once == 0 && ScanHugochan.activeSelf == false && ScanMadeTea.activeSelf == false) 
		{
			Forcus.unShowImage ();
			Scanlogo.SetActive (true);
			timestart = true;
			play_once = 1;
		} 
		if (Hugo.activeSelf && play_once == 0 && Scanlogo.activeSelf == false && ScanMadeTea.activeSelf == false)
		{
			Forcus.unShowImage ();
			ScanHugochan.SetActive (true);
			time_1_start = true;
			tea_2_ani.enabled = true;
			play_once = 1;
		}
		if (MadeTea.activeSelf && play_once == 0 && Scanlogo.activeSelf == false && ScanHugochan.activeSelf == false)
		{
			Forcus.unShowImage ();
			ScanMadeTea.SetActive (true);
			time_2_start = true;
			play_once = 1;
		}
		if (TeaVideo.activeSelf && play_once == 0 && Scanlogo.activeSelf == false && ScanHugochan.activeSelf == false)
		{
			Forcus.unShowImage ();
			ScanVideo.SetActive (true);
			scrMedia.Load("tea.mp4");
			time_3_start = true;
			play_once = 1;
		}


			
		if (timestart)
		{
			quit.SetActive (false);
			time = time + 1f * Time.deltaTime;
			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began)
			{
				Timeout.SetActive (true);
				Time.timeScale = 0;
				music_1.Pause ();
			}
		}
		if (time_1_start)
		{
			quit.SetActive (false);
			time_1 = time_1 + 1f * Time.deltaTime;
			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) 
			{
				Timeout.SetActive (true);
				Time.timeScale = 0;
				music_2.Pause ();
			}
		}
		if (time_2_start)
		{
			quit.SetActive (false);
			time_2 = time_2 + 1f * Time.deltaTime;
			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) 
			{
				Timeout.SetActive (true);
				Time.timeScale = 0;
				music_3.Pause ();
			}
		}
		if (time_3_start)
		{
			quit.SetActive (false);
			time_3 = time_3 + 1f * Time.deltaTime;
			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) 
			{
				Timeout.SetActive (true);
				scrMedia.Pause();
			}
		}



		if (time >= 15f && timestart == true)
		{
			quit.SetActive (true);
			shot.SetActive (true);
			replay.SetActive (true);
			back.SetActive (true);
			timestart = false;
		}
		if (time_1 >= 32f)
		{
			quit.SetActive (true);
			replay_1.SetActive (true);
			back.SetActive (true);
			time_1_start = false;
			tea_2_ani.enabled = false;
		}
		if (time_2 >= 46f)
		{
			quit.SetActive (true);
			replay_2.SetActive (true);
			back.SetActive (true);
			time_2_start = false;
		}
		if (time_3 >= 110f)
		{
			quit.SetActive (true);
			replay_3.SetActive (true);
			back.SetActive (true);
			time_3_start = false;
			scrMedia.UnLoad();
		}
	}
	public void continue_fn ()
	{
		Time.timeScale = 1;
		Timeout.SetActive (false);
		music_1.UnPause ();
		music_2.UnPause ();
		music_3.UnPause ();
		if (ScanVideo.activeSelf)
		{
			scrMedia.Play();
		}
	}
	public void back_fn ()
	{
		Scanlogo.SetActive (false);
		ScanHugochan.SetActive (false);
		ScanMadeTea.SetActive (false);
		ScanVideo.SetActive (false);
		shot.SetActive (false);
		replay.SetActive (false);
		replay_1.SetActive (false);
		replay_2.SetActive (false);
		replay_3.SetActive (false);
		back.SetActive (false);
		Timeout.SetActive (false);
		scrMedia.UnLoad();
		play_once = 0;
		time = 0;
		time_1 = 0;
		time_2 = 0;
		time_3 = 0;
		if (logo.activeSelf == false && Hugo.activeSelf == false && MadeTea.activeSelf == false && TeaVideo.activeSelf == false)
		{
			Forcus.ShowImage ();
		}
	}
	public void back_1_fn ()
	{
		Scanlogo.SetActive (false);
		ScanHugochan.SetActive (false);
		ScanMadeTea.SetActive (false);
		ScanVideo.SetActive (false);
		shot.SetActive (false);
		replay.SetActive (false);
		replay_1.SetActive (false);
		replay_2.SetActive (false);
		replay_3.SetActive (false);
		back.SetActive (false);
		Timeout.SetActive (false);
		timestart = false;
		time_1_start = false;
		time_2_start = false;
		time_3_start = false;
		quit.SetActive (true);
		scrMedia.UnLoad();
		play_once = 0;
		time = 0;
		time_1 = 0;
		time_2 = 0;
		time_3 = 0;
		Time.timeScale = 1;
		if (logo.activeSelf == false && Hugo.activeSelf == false && MadeTea.activeSelf == false && TeaVideo.activeSelf == false)
		{
			Forcus.ShowImage ();
		}
	}
	public void Shot ()
	{
		shot.SetActive (false);
		replay.SetActive (false);
		back.SetActive (false);
		quit.SetActive (false);
		StartCoroutine (TakeShot());
	}
	public void Replay_fn ()
	{
		Scanlogo.SetActive (false);
		Scanlogo.SetActive (true);
		timestart = true;
		shot.SetActive (false);
		replay.SetActive (false);
		back.SetActive (false);
		time = 0;
	}
	public void Replay_1_fn ()
	{
		ScanHugochan.SetActive (false);
		ScanHugochan.SetActive (true);
		time_1_start = true;
		replay_1.SetActive (false);
		back.SetActive (false);
		tea_2_ani.enabled = true;
		time_1 = 0;
		play_once = 0;
	}
	public void Replay_2_fn ()
	{
		ScanMadeTea.SetActive (false);
		ScanMadeTea.SetActive (true);
		time_2_start = true;
		replay_2.SetActive (false);
		back.SetActive (false);
		time_2 = 0;
		play_once = 0;
	}
	public void Replay_3_fn ()
	{
		scrMedia.Load("tea.mp4");
		time_3_start = true;
		replay_3.SetActive (false);
		back.SetActive (false);
		time_3 = 0;
		play_once = 0;
	}

	public void Quit ()
	{
		Application.Quit();
	}
		

//	private IEnumerator TakeScreenShotCo()
//	{
//		yield return new WaitForEndOfFrame();
//
//		DirectoryInfo _DirInfo = new DirectoryInfo (Application.dataPath);
//		DirectoryInfo _SaveDirInfo = new DirectoryInfo (Path.Combine (_DirInfo.Parent.FullName, "Screenshot"));
//		//如果搜尋不到指定資料夾則創建新資料夾
//		if (!_SaveDirInfo.Exists)
//			_SaveDirInfo.Create ();
//
//		FileInfo _SaveFileInfo = new FileInfo (Path.Combine (_SaveDirInfo.FullName, string.Format ("Screenshot {0:yy-MM-dd HH-mm-ss}.png", System.DateTime.Now)));
//
//		//superSize為圖片解析度倍數相乘
//		ScreenCapture.CaptureScreenshot (_SaveFileInfo.FullName,superSize);
//
//
//	}
	private IEnumerator TakeShot()
	{
		yield return new WaitForEndOfFrame();
		System.DateTime now = System.DateTime.Now; 
		string times = now.ToString (); 
		times = times.Trim (); 
		times = times.Replace ("/","-"); 
		//文件名
		string filename = "Screenshot"+times+".png"; 

		Texture2D texture = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false); 
		texture.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0); 
		texture.Apply (); 
		//转为字节数组 
		byte[] bytes = texture.EncodeToPNG ();
		string destination = "/sdcard/DCIM/Hugo"; 
		//判断目录是否存在，不存在则会创建目录 
		if (!Directory.Exists (destination)) 
		{ 
			Directory.CreateDirectory (destination); 
		} 
		//文件路径
		string Path_save = destination + "/" + filename;
		//存图片 
		System.IO.File.WriteAllBytes (Path_save, bytes);

		yield return new WaitForSeconds(1);
		shot.SetActive (true);
		replay.SetActive (true);
		back.SetActive (true);
		quit.SetActive (true);
		hint.SetActive (true);
		yield return new WaitForSeconds(5);
		hint.SetActive (false);
	}
}
